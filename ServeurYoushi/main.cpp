#include <iostream>
#include <pthread.h>
#include <time.h>
#include <winsock2.h>
typedef int socklen_t;

#define NBRarbre 15
#define MOUTON 7
#define PORT 23

// Fonction pour envoyer un entier sur le r�seau
int EnvoyerEntier(SOCKET sock, int16_t value)
{
	int16_t sendValue = htons(value);
	return send(sock, (char const*)&sendValue, sizeof sendValue, 0);
}

// Fonction qui re�oit un entier sur le r�seau
int RecevoirEntier(SOCKET sock, int16_t * pValue)
{
	int16_t recvValue = 0;
	int ret = recv(sock, (char *)&recvValue, sizeof recvValue, 0);
	*pValue = ntohs(recvValue);
	return ret;
}

// Structure qui contient des positions x et y
struct position
{
    int16_t x, y;
};

// Structure qui contient les coordon�es et l'�tat des personnages
struct personnage
{
    int16_t x, y;
    bool etat;
};

// Sructure g�n�ral qui regroupe les structures pr�c�dentes pour chaque arbre et personnages ainsi que les sockets (interface r�seau)
struct general
{
    struct personnage j1, j2;
    struct position arbre[NBRarbre];
    int sock1, sock2;
};

// Fonction qui s'occupe de monter les arbres et de g�rer le mouton
void deplacementArbre(struct  position arbre[])
{
    bool continuer = true;
    double wait = 200; // Variable pour g�rer l'acc�l�ration
    char valeur_depl_arbres = 2; // Variable pour g�rer la vitesse des arbres

    bool droite = true; // Variable qui d�termine si le outon va � gauche ou � droite
    while(continuer)
    {
        int j;
        for (j=0; j<NBRarbre; j++) // D�placement de tous les arbres vers le haut
        {
            arbre[j].y -= valeur_depl_arbres;
            if(arbre[MOUTON].y <0) // Si c'est le mouton et qu'il est en haut de l'�cran alors le ree sois � gauche sois � droite
            {
                if (rand()%2)
                {
                    arbre[MOUTON].x = 0;
                }
                else
                {
                    arbre[MOUTON].x = 250;
                }
            }
            if(arbre[j].y <0) // Si c'est un arbre qui est en haut de l'�cran alors le remet en bas avec une position al�atoire
            {
                arbre[j].x = rand()%380;
                arbre[j].y = 599;
            }
        }
        Sleep(wait); // Ralenti le jeu avec la valeur d'acc�l�ration
        if(wait>0) // Si l'acc�l�ration est positive alors r�duit sa valeur pour ne pas avoir une acc�l�ration exponentielle
        {
            if(wait > 100)
            wait *= 0.85;

            else if(wait > 60)
            {
                valeur_depl_arbres = 3;
                wait *= 0.98;
            }
            else if(wait > 30)
            {
                valeur_depl_arbres = 4;
                wait *= 0.992;
            }
            else if (wait > 15)
            {
                valeur_depl_arbres = 5;
                wait *= 0.998;
            }
            else if (wait > 10)
            {
                valeur_depl_arbres = 6;
                wait *= 0.9998;
            }
            else if (wait > 5)
            {
                valeur_depl_arbres = 8;
                wait *= 0.999992;
            }

        }
        // D�placement du mouton � droite ou � gauche
        if(arbre[MOUTON].x <= 1 || (arbre[MOUTON].x <= 250 && arbre[MOUTON].x > 242))
            droite = true;
        else if((arbre[MOUTON].x > 109 && arbre[MOUTON].x < 118) || arbre[MOUTON].x > 359)
            droite = false;

        if(droite)
            arbre[MOUTON].x+=valeur_depl_arbres;
        else
            arbre[MOUTON].x-=valeur_depl_arbres;

        Sleep(6);
    }

}
// Thread qui s'occupe du client 1
void* client1(void* data)
{
    std::cout << "Thread 1 pret" << std::endl;
    // On r�cup�re la structure g�n�rale
    struct general *letout;
    letout = (struct general*) data;

    int i = 0;
    for(i=0; i<NBRarbre; i++) // Envoie des coordonn�es de tout les arbres
    {
        EnvoyerEntier(letout->sock1, letout->arbre[i].x);
        EnvoyerEntier(letout->sock1, letout->arbre[i].y);
    }

    letout->j1.x = 133;
    letout->j1.y = 0;
	// Envoie des coordon�es des joueurs
    EnvoyerEntier(letout->sock1, letout->j1.x);
    EnvoyerEntier(letout->sock1, letout->j2.x);

    while (letout->j1.etat || letout->j2.etat) // Tant que les deux joueurs sont vivant
    {
        RecevoirEntier(letout->sock1, &letout->j1.x); // Recoit la position du joueur 1

        EnvoyerEntier(letout->sock1, letout->j2.x); // Envoi au joueur 1 la position du joueur 2
        for(i=0; i<NBRarbre; i++) // Envoie les coordon�es des arbres
        {
            EnvoyerEntier(letout->sock1, letout->arbre[i].x);
            EnvoyerEntier(letout->sock1, letout->arbre[i].y);
        }
        if(letout->j1.x == 500){letout->j1.etat = false;} // Si la position du joueur 1 est hors de l'�cran alors le consid�re comme mort
        Sleep(20); // Ralenti pour se synchroniser avec le client
    }

    shutdown(letout->sock1, 2); // Ferme la connection

    closesocket(letout->sock1); // Supprime la socket
}

// Thread pour le client 2
void* client2(void* data)
{
    std::cout << "Thread 2 pret" << std::endl;
    // On r�cup�re la structure g�n�ral
    struct general *letout;
    letout = (struct general*) data;

    int i = 0;
    for(i=0; i<NBRarbre; i++) // Envoie des coordon�es des arbres
    {
        EnvoyerEntier(letout->sock2, letout->arbre[i].x);
        EnvoyerEntier(letout->sock2, letout->arbre[i].y);
    }

    letout->j2.x = 200;
    letout->j2.y = 0;

	// Envoi des coordonn�es des joueurs
    EnvoyerEntier(letout->sock2, letout->j2.x);
    EnvoyerEntier(letout->sock2, letout->j1.x);

    while (letout->j2.etat || letout->j1.etat) // Tant que les deux joueurs son vivant
    {
        RecevoirEntier(letout->sock2, &letout->j2.x); // Recoit la position du joueur 2
        EnvoyerEntier(letout->sock2, letout->j1.x); // Envoi au joueur 2 la position du joueur 1
        for(i=0; i<NBRarbre; i++) // Envoie les coordonn�es des arbres
        {
            EnvoyerEntier(letout->sock2, letout->arbre[i].x);
            EnvoyerEntier(letout->sock2, letout->arbre[i].y);
        }
        if(letout->j2.x == 500){letout->j2.etat = false;} // Si le joueur 2 est hors de l'�cran alors le consid�re comme mort
        Sleep(20); // Ralenti le serveur pour le synchroniser avec le client
    }

    shutdown(letout->sock2, 2); // Ferme la connection 2
    closesocket(letout->sock2); // Ferme la socket 2
}

int main()
{
    srand(time(NULL));   // Initialisation de l'al�atoire
    // Initialisation des sockets
    std::cout << "Initialisation des sockets... ";
    WSADATA wsa;
    WSAStartup(MAKEWORD(2, 2), &wsa);


    // Cr�ation et initialisation des arbres
    std::cout << "Generation des arbres... ";
    struct general letout;

    int i;
    for (i=0; i<NBRarbre; i++) // G�n�re les arbres
    {
        letout.arbre[i].x = rand()%400;
        letout.arbre[i].y = i*40+80;
    }

    std::cout << "done" << std::endl;

    // Socket et contexte d'adressage du serveur
    SOCKADDR_IN sin;
    SOCKET sock;
    socklen_t recsize = sizeof(sin);

    //On cr�e une socket
    std::cout << "Creation et initialisation de la socket 1... " << std::endl;
    sock = socket(AF_INET, SOCK_STREAM, 0);

    // On initialise la socket
    sin.sin_addr.s_addr = htonl(INADDR_ANY);
    sin.sin_family = AF_INET;
    sin.sin_port = htons(23);
    if(sock != INVALID_SOCKET){std::cout << "Socket serveur cree et initialise." << std::endl;}
    else{std::cout << "Socket serveur non cr�e";}

    if(bind(sock, (SOCKADDR*)&sin, sizeof(sin)) == SOCKET_ERROR)
    {
        std::cout << "Bind socket serveur error." << std::endl;
    }

    // Cr�ation contexte adressage clients
    SOCKADDR_IN csin;
    socklen_t crecsize = sizeof(csin);

	// On d�sactive l'attente entre chaque envoi de donn�es pour �viter les lags
    int flag = 1;
    int result = setsockopt(sock, IPPROTO_TCP, TCP_NODELAY, (char *) &flag, sizeof(int));

    // On �coute sur le port
    std::cout << "Socket serveur en ecoute... " << std::endl;
    listen(sock, 2);
    socklen_t taille = sizeof(csin);
	// Accepte le client 1
    letout.sock1 = accept(sock, (SOCKADDR*)&csin, &taille);
    std::cout << "Socket 1 accepte" << std::endl;
	// Accepte le client 2
    letout.sock2 = accept(sock, (SOCKADDR*)&csin, &taille);
    std::cout << "Socket 2 accepte" << std::endl;

    // Cr�ation des thread pour la liason avec un client
    pthread_t thread, thread2;
    pthread_create(&thread, NULL, client1, (void*)&letout);
    pthread_create(&thread2, NULL, client2, (void*)&letout);

    Sleep(3000); // Attente de 3 secondes que le d�compte se fasse sur le client
    deplacementArbre(letout.arbre);

	// On attend la fin des connections entre les clients
    pthread_join(thread, NULL);
    pthread_join(thread2, NULL);
    std::cout << "Fermeture du serveur...";
    WSACleanup();
    return 0;
}
