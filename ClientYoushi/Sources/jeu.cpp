#include <iostream>
#include <stdio.h>
#include <string>
#include "allegro5/allegro.h"
#include "allegro5/allegro_image.h"
#include "allegro5/allegro_primitives.h"
#include <allegro5/allegro_font.h>
#include <allegro5/allegro_ttf.h>
#include <allegro5/allegro_audio.h>
#include <allegro5/allegro_acodec.h>
#include <pthread.h>
#include "time.h"
#include "jeu.h"

#define YOUSHI 0
#define LEIRA 1
#define QUITTER 2
#define MOUTON 7

#define NbArbre 15


#include <winsock2.h>
typedef int socklen_t;
#define PORT 23

using namespace std;


char selection_perso(ALLEGRO_DISPLAY* display) {
    char perso;                                                  // Initialisations
    bool fleche_perso=0;
    ALLEGRO_BITMAP  *selectPerso  = NULL;
    ALLEGRO_BITMAP  *fleche  = NULL;
    ALLEGRO_BITMAP  *youshiHighLight = NULL;
    ALLEGRO_BITMAP  *leiraHighLight = NULL;
    ALLEGRO_EVENT_QUEUE *queuePerso = NULL;

    youshiHighLight = al_load_bitmap("selectplayer(Youshi highlighted).png");
    leiraHighLight = al_load_bitmap("selectplayer(Leira highlighted).png");
    fleche = al_load_bitmap("fleche_verticale.png");
    selectPerso = al_load_bitmap("selectplayer.png");
    if(!selectPerso) {
        cout << "Failed to display selectPerso" << endl;
   }

   queuePerso = al_create_event_queue();
   if(!queuePerso) {
      cout << "Failed to create QueuePerso" << endl;
   }

   al_register_event_source(queuePerso, al_get_keyboard_event_source());           // Les events seront du clavier...
   al_register_event_source(queuePerso, al_get_mouse_event_source());              // De la souris...
   al_register_event_source(queuePerso, al_get_display_event_source(display));     // Ou de la fen�tre.
    ALLEGRO_EVENT eventPerso;

   al_draw_bitmap(selectPerso,0,0,0);                       // On buffer l'�cran de s�lection des personnages
   al_draw_bitmap(fleche, 107, 335, 0);                     // On buffer la fl�che de s�lection sous Youshi, choix par d�faut
   al_flip_display();                                       // On affiche

   bool done = false;
    while(!done)
    {
        al_wait_for_event(queuePerso, &eventPerso);                         // On attend un �v�nement
        if (eventPerso.type == ALLEGRO_EVENT_KEY_DOWN)                      // Si on appuie sur une touche
        {
            al_draw_bitmap(selectPerso,0,0,0);                              // On efface l'ancienne fl�che
            if (eventPerso.keyboard.keycode == ALLEGRO_KEY_LEFT){           // On affiche la fl�che sous Youshi si on appuie sur la fl�che gauche
                al_draw_bitmap(fleche, 107, 335, 0);
                fleche_perso = 0;
            }
            else if (eventPerso.keyboard.keycode == ALLEGRO_KEY_RIGHT){     // On affiche la fleche sous Leira si on appuie sur la fl�che droite
                al_draw_bitmap(fleche, 261, 335, 0);
                fleche_perso = 1;
            }
            else if (eventPerso.keyboard.keycode == ALLEGRO_KEY_ENTER)      // Si c'est "enter", on choisit le personnage sous lequel est la fl�che et on quitte la boucle
            {
                switch (fleche_perso)
                {
                    case 0:
                        perso = YOUSHI;
                        done = true;
                        break;
                    case 1:
                        perso = LEIRA;
                        done = true;
                        break;
                }
            }
            else if (eventPerso.keyboard.keycode == ALLEGRO_KEY_ESCAPE)     // Si c'est "echap", on quitte la boucle et on ferme le jeu
            {
                perso = QUITTER;
                done = true;
            }
            al_flip_display();                                              // On affiche la nouvelle position de la fl�che si besoin
        }
        else if (eventPerso.mouse.x >= 55 && eventPerso.mouse.x <= 179 && eventPerso.mouse.y >= 171 && eventPerso.mouse.y <= 321)   // Si le curseur est dans le cadre de Youshi
        {
            al_draw_bitmap(selectPerso,0,0,0);
            al_draw_bitmap(fleche, 107, 335, 0);                        // On buffer le cadre de Youshi
            al_flip_display();                                          // On l'affiche
            if (eventPerso.type == ALLEGRO_EVENT_MOUSE_BUTTON_UP)       // Si on clique dans le cadre
            {
                perso = YOUSHI;                                         // On s�lectionne Youshi
                done = true;
            }
        }
        else if (eventPerso.mouse.x >= 212 && eventPerso.mouse.x <= 330 && eventPerso.mouse.y >= 171 && eventPerso.mouse.y <= 321)  // Si le curseur est dans le cadre de Leira
        {
            al_draw_bitmap(selectPerso,0,0,0);
            al_draw_bitmap(fleche, 261, 335, 0);                        // M�me processus
            al_flip_display();
            if (eventPerso.type == ALLEGRO_EVENT_MOUSE_BUTTON_UP)
            {
                perso = LEIRA;                                          // On s�lectionne Leira
                done = true;
            }
        }
        else if(eventPerso.type == ALLEGRO_EVENT_DISPLAY_CLOSE)         // Si on ferme la fen�tre avec le croix rouge
        {
            perso = QUITTER;                                            // On quitte la boucle, puis le jeu
            done = true;
        }
    }
    cout << perso << endl;
    cout << "perso is upon me" << endl;

    al_destroy_bitmap(selectPerso);                     // Destruction des images utilis�es dans la fonction selectionPerso
    al_destroy_bitmap(fleche);
    al_destroy_bitmap(youshiHighLight);
    al_destroy_bitmap(leiraHighLight);
    al_destroy_event_queue(queuePerso);
    return perso;                                       // On retourne le personnage choisi, ou si on quitte le jeu
}

// La fonction principale du jeu.

void jeu(int perso, ALLEGRO_DISPLAY* display) {
    ALLEGRO_BITMAP *img_arbre = NULL, *sprite_perso1 = NULL, *sprite_perso2 = NULL, *sol = NULL, *img_mouton = NULL;
    ALLEGRO_TIMER* timer;                                                       // On initialise un timer
    ALLEGRO_FONT *font = al_load_ttf_font("Winter flakes.ttf",50,0);            // On initialise la police d'�criture du score
    ALLEGRO_SAMPLE *sample_1=NULL, *sample_2=NULL, *sample_3=NULL, *sample_go=NULL, *sample_Lliendes=NULL; // Pr�paration pour le stockage des sons
    ALLEGRO_EVENT_QUEUE *event_queue = NULL;
    ALLEGRO_EVENT event;
    event_queue = al_create_event_queue();                                      // On cr�e une queue d'�v�nements
    timer = al_create_timer(1.0);                                               // On cr�e un timer
    bool continuer = true;
    int score = 0, score_j1 = 0, score_j2 = 0;
    char str_score[6];
    img_arbre = al_load_bitmap("arbre.png");                                    // On charge les images
    sol = al_load_bitmap("sol.png");
    img_mouton = al_load_bitmap("mouton.png");
    sample_1 = al_load_sample( "1 [English].wav" );                             // Chargement
    sample_2 = al_load_sample( "2 [English].wav" );                             // Des sons
    sample_3 = al_load_sample( "3 [English].wav" );
    sample_go = al_load_sample( "Go [English].wav" );
    sample_Lliendes = al_load_sample("Xera_-_Lliendes_accelerated.wav");        // Et de la musique
    if(!img_arbre) {
        cout << "Failed to display img_arbre" << endl;
    }

    al_register_event_source(event_queue, al_get_keyboard_event_source());          // On g�re les events du clavier
    al_register_event_source(event_queue, al_get_display_event_source(display));    // Et de la fen�tre
    cout << "1" << endl;

    struct general evthg;                                       // On cr�e everything, une structure g�n�rale
    evthg.j1.posx = 0;
    evthg.j1.etat = true;
    evthg.j2.etat = true;
    evthg.j1.posy = 0;
    evthg.j2.posy = 0;

    // Initialisation des sockets
    WSADATA wsa;
    WSAStartup(MAKEWORD(2, 2), &wsa);


    // Cr�ation du thread d�di� au r�seau
    pthread_t thread;
    pthread_create(&thread, NULL, reseau, (void*)&evthg);

    while(evthg.j1.posx == 0) // Tant que le deuxi�me joueur n'est pas connect�, attendre
    {
        Sleep(10);
    }

    // Le choix des persos est g�r� en interne. Si un joueur prend Youshi, alors l'autre peut aussi le prendre.
    // Seules les coordonn�es sont envoy�es au serveur. Le choix du sprite est celui du joueur.

    if (perso)                                                  // Si perso = vrai = 1 = LEIRA (les femmes d'abord !)
    {
        sprite_perso1 = al_load_bitmap("leiraneige.png");       // Le sprite du j1 est Leira
        sprite_perso2 = al_load_bitmap("youshineige.png");      // Le j2 est Youshi
    }
    else                                                        // Sinon, on affiche Youshi
    {
        sprite_perso1 = al_load_bitmap("youshineige.png");
        sprite_perso2 = al_load_bitmap("leiraneige.png");
    }

    // Passage un peu technique pour afficher un d�compte de 3 secondes avant le lancement du jeu.
    int j=0;
    int k=3;
    char str_k[3];
    while (k>0)
    {
        al_draw_bitmap(sol,0,0,0);                                      // On affiche la map
        al_draw_bitmap(sprite_perso1,evthg.j1.posx,evthg.j1.posy,0);    // Et les persos
        al_draw_bitmap(sprite_perso2,evthg.j2.posx,evthg.j2.posy,0);    // Pour effacer l'ancien chiffre

        for(j=0;j<NbArbre;j++)           // On affiche les arbres et le mouton
        {
            if (j==MOUTON)
            {
                if (rand()%2)
                {
                    al_draw_bitmap(img_mouton, 0, evthg.arbre[j].posy,0);
                }
                else
                {
                    al_draw_bitmap(img_mouton, 250, evthg.arbre[j].posy,0);
                }
            }
            else
            {
                al_draw_bitmap(img_arbre, evthg.arbre[j].posx, evthg.arbre[j].posy,0);
            }
        }
        // Comme al_draw_text ne peut afficher que des char, on convertit k en char
        sprintf(str_k, "%d", k);
        switch(k)
        {
            case 1:
            al_play_sample(sample_1, 1.0, 0.0,1.0,ALLEGRO_PLAYMODE_ONCE,NULL); // On joue le d�compte oral avant le d�but du jeu
            break;
            case 2:
            al_play_sample(sample_2, 1.0, 0.0,1.0,ALLEGRO_PLAYMODE_ONCE,NULL);
            break;
            case 3:
            al_play_sample(sample_3, 1.0, 0.0,1.0,ALLEGRO_PLAYMODE_ONCE,NULL);
            break;
        }
        al_draw_text(font, al_map_rgb(0,0,0), 200, 200,ALLEGRO_ALIGN_CENTRE, str_k); // On affiche le d�compte
        al_flip_display();
        al_rest(1);                                                                  // On attend une seconde
        k--;                                                                         // On d�cr�mente k
    }

    al_play_sample(sample_go, 1.0, 0.0,1.0,ALLEGRO_PLAYMODE_ONCE,NULL);     // Le jeu se lance en m�me temps que "Go !" est jou�

    al_start_timer(timer);                  // On lance le timer


    // On arrive � la boucle principale du programme : le coeur du jeu.
    // Tant que les 2 joueurs sont vivants et qu'on ne quitte pas le jeu, on appelle toutes les fonctions.
    //----------------------------------------------------------------------

    while ((evthg.j1.etat || evthg.j2.etat) && continuer)
    {
	if (score < 1)
        al_play_sample(sample_Lliendes, 1.0, 0.0, 1.0, ALLEGRO_PLAYMODE_LOOP,NULL);         // On lance la musique du jeu

        continuer = deplacer(display, event_queue, &event, &evthg.j1, &evthg.j2, continuer, &score, &score_j1);
        score_j1 = collision(&evthg, score, score_j1);
        afficher(display, &evthg, img_arbre, sprite_perso1, sprite_perso2, sol, img_mouton);
        score = check_timer(timer, score);                                                  // On check le timer pour avoir le score
        if(evthg.j2.posx == 500 && evthg.j2.etat == true)
        {
            evthg.j2.etat = false;
            score_j2 = score;
        }
        sprintf(str_score, "%d", score);                                                    // On convertit le score en chaine de caract�res
        al_draw_text(font, al_map_rgb(0,0,0), 325, 35,ALLEGRO_ALIGN_LEFT, str_score);       // On buffer le score
        al_flip_display();                                                                  // On affiche tout
        al_rest(0.0166666666666667);                                                        // On attend 1/60 seconde. On a ainsi 60 FPS.
        if(evthg.arbre[1].posy == evthg.arbre[2].posy)
        {
            evthg.j2.etat = false;
            score_j2 = score;
        }
    }

    //-----------------------------------------------------------------------

    al_stop_samples();
    al_stop_timer(timer);                           // On arr�te le timer
    game_is_over(display, score, score_j1, score_j2);         // On appellle l'�cran de fin
    al_destroy_bitmap(sprite_perso1);               // On d�truit tout ce qui a �t� appell� : images, timer, police, queue d'�v�nements, sons.
    al_destroy_bitmap(sprite_perso2);
    al_destroy_bitmap(sol);
    al_destroy_bitmap(img_arbre);
    al_destroy_bitmap(img_mouton);
    al_destroy_sample(sample_1);
    al_destroy_sample(sample_2);
    al_destroy_sample(sample_3);
    al_destroy_sample(sample_go);
    al_destroy_sample(sample_Lliendes);
    al_destroy_font(font);
    al_destroy_timer(timer);
    al_destroy_event_queue(event_queue);


    WSACleanup(); // Fin du contexte r�seau


}

bool deplacer(ALLEGRO_DISPLAY* display, ALLEGRO_EVENT_QUEUE *event_queue, ALLEGRO_EVENT *event, struct persos *j1, struct persos *j2, bool continuer, int *score, int *score_j1)
{
    ALLEGRO_KEYBOARD_STATE keyboard_state;
    while(al_get_next_event(event_queue, event))            // On recherche un �v�nement parmi toute la queue d'events
        {
            if(event->type == ALLEGRO_EVENT_DISPLAY_CLOSE)  // Si on ferme la fen�tre avec la croix rouge
            {
                continuer = false;                          // On quitte le jeu pour arriver � l'�cran de mort
                *score_j1 = *score;
            }
        }

    // Pour le d�placement du joueur, on ne bloque pas le jeu avec un al_wait_event
    // A la place, on check l'�tat du clavier � l'instant donn�
    al_get_keyboard_state(&keyboard_state);

    if(al_key_down(&keyboard_state, ALLEGRO_KEY_ESCAPE))                            // Si "echap" est enfonc�, on quitte le jeu
        {
            continuer = false;
            *score_j1 = *score;
        }
        else if(al_key_down(&keyboard_state, ALLEGRO_KEY_LEFT) && j1->posx > 0)     // Si c'est la fl�che gauche, on d�place le joueur � gauche
        {
            if(*score > 50)
                j1->posx-=4;
            j1->posx-=3;
        }
        else if(al_key_down(&keyboard_state, ALLEGRO_KEY_RIGHT) && j1->posx < 384)  // Idem pour la fl�che droite.
        {
            if(*score > 50)
                j1->posx+=4;
            j1->posx+=3;
        }
        return continuer;                         // On renvoie si on continue le jeu ou pas.
}


// Ici, on v�rifie que le personnage n'est pas en collision avec un arbre, auquel cas il meurt.
int collision(struct general *evthg, int score, int score_j1)
{
    int i=0;
    for(i=0;i<NbArbre;i++)
    {
        if (evthg->arbre[i].posy <= 10 && (evthg->j1.posx < evthg->arbre[i].posx+26) && (evthg->arbre[i].posx-4 < evthg->j1.posx))
        {
            evthg->j1.etat = false;
            evthg->j1.posx = 500;
            score_j1 = score;
        }
    }
    return score_j1;
}

void afficher(ALLEGRO_DISPLAY* display, struct general *evthg, ALLEGRO_BITMAP* img_arbre, ALLEGRO_BITMAP* sprite_perso1, ALLEGRO_BITMAP* sprite_perso2, ALLEGRO_BITMAP* sol, ALLEGRO_BITMAP* img_mouton)
{
    al_draw_bitmap(sol,0,0,0);          // On buffer la map pour effacer les anciennes positions

    int i=0;
    for(i=0;i<NbArbre;i++)                   // On buffer les 14 arbres et le mouton
    {
        if (i==MOUTON)
        {
            al_draw_bitmap(img_mouton, evthg->arbre[i].posx, evthg->arbre[i].posy,0);
        }
        else
        {
            al_draw_bitmap(img_arbre, evthg->arbre[i].posx, evthg->arbre[i].posy,0);
        }
    }
    if(evthg->j2.etat){al_draw_bitmap(sprite_perso2, evthg->j2.posx, evthg->j2.posy,0);}    // On buffer j2 et j1
    if(evthg->j1.etat){al_draw_bitmap(sprite_perso1, evthg->j1.posx, evthg->j1.posy,0);}
}

int check_timer(ALLEGRO_TIMER* timer, int score)
{
    int time = 0;
    time = al_get_timer_count(timer);
    if (time > 1.0)                         // Toutes les secondes, on incr�mente le score
    {
        score++;
        al_add_timer_count(timer, -1.0);
    }
    return score;                           // On retourne le score
}

void game_is_over(ALLEGRO_DISPLAY* display, int score, int score_j1, int score_j2)
{
    bool exit = false;                      // Initialisation de variables, de l'image, de la police d'�criture, de la queue d'events et de la musique
    char str_score[6];
    char str_score_j1[6];
    char str_score_j2[6];
    ALLEGRO_BITMAP *death_screen = NULL;
    ALLEGRO_SAMPLE *death_music = NULL;
    ALLEGRO_FONT *font = al_load_ttf_font("evanescent.ttf",50,0);
    ALLEGRO_EVENT_QUEUE *event_queue = NULL;

    death_music = al_load_sample( "John Murphy - Surface Of The Sun (Sunshine OST).wav" );  // On joue la musique de mort
    death_screen = al_load_bitmap("death_screen.png");                              // On charge l'�cran de mort
    event_queue = al_create_event_queue();                                          // On cr�e la queue
    al_register_event_source(event_queue, al_get_keyboard_event_source());          // On g�re les events du clavier
    al_register_event_source(event_queue, al_get_display_event_source(display));    // Et de la fen�tre
    ALLEGRO_EVENT event;

    al_draw_bitmap(death_screen,0,0,0);                                             // On buffer l'�cran de mort
    sprintf(str_score, "%d", score);                                                // On convertit en char les scores des joueurs
    sprintf(str_score_j1, "%d", score_j1);
    sprintf(str_score_j2, "%d", score_j2);

    // On buffer l'affichage des scores et de d�faite/victoire.
    al_draw_text(font, al_map_rgb(255,255,255), 150, 400,ALLEGRO_ALIGN_CENTRE, "Votre Score : ");
    if (score > score_j1)
    {
        al_draw_text(font, al_map_rgb(255,255,255), 200, 150,ALLEGRO_ALIGN_CENTRE, "Defaite !");
        al_draw_text(font, al_map_rgb(255,255,255), 300, 400,ALLEGRO_ALIGN_CENTRE, str_score_j1);
        al_draw_text(font, al_map_rgb(255,255,255), 150, 450,ALLEGRO_ALIGN_CENTRE, "Score J2 : ");
        al_draw_text(font, al_map_rgb(255,255,255), 300, 450,ALLEGRO_ALIGN_CENTRE, str_score_j2);
    }
    else
    {
        al_draw_text(font, al_map_rgb(255,255,255), 200, 150,ALLEGRO_ALIGN_CENTRE, "Victoire !");
        al_draw_text(font, al_map_rgb(255,255,255), 300, 400,ALLEGRO_ALIGN_CENTRE, str_score_j1);
        al_draw_text(font, al_map_rgb(255,255,255), 150, 450,ALLEGRO_ALIGN_CENTRE, "Score J2 : ");
        al_draw_text(font, al_map_rgb(255,255,255), 300, 450,ALLEGRO_ALIGN_CENTRE, str_score_j2);
    }

    al_flip_display();                  // On affiche
    al_play_sample(death_music, 1.0, 0.0,1.0,ALLEGRO_PLAYMODE_LOOP,NULL);

    al_rest(1);                         // On attend une seconde

    while(!exit)                        // Il suffit d'appuyer sur n'importe quelle touche du clavier ou de fermer
    {                                   // la fen�tre pour quitter le jeu.
        al_wait_for_event(event_queue, &event);
        if(event.type == ALLEGRO_EVENT_KEY_DOWN || event.type == ALLEGRO_EVENT_DISPLAY_CLOSE)
        {
            exit = true;
            al_stop_samples();                  // On arr�te la musique
            al_destroy_bitmap(death_screen);    // On d�truit images, musique, police, queue d'�v�nements.
            al_destroy_sample(death_music);
            al_destroy_font(font);
            al_destroy_event_queue(event_queue);
        }
    }
}



int EnvoyerEntier(SOCKET sock, int16_t value)
{
	int16_t sendValue = htons(value); /* variable de reception est codee en hton sur 16bits */

	return send(sock, (char const*)&sendValue, sizeof sendValue, 0); //retourne la variable
}
/* fonction de reception d'une variable entiere codee sur 16bits par le socket (1er parametre) */

int RecevoirEntier(SOCKET sock, int16_t * pValue)
{
	int16_t recvValue = 0; // variable en codee sur 16bits
	int ret = recv(sock, (char *)&recvValue, sizeof recvValue, 0);
	*pValue = ntohs(recvValue); /* code la variable en hton */
	return ret;
}


void* reseau(void* data)
{
    struct general *evthg;                  // on cr�e une structure reprenant la structure generae
    evthg = (struct general*) data;

                                                    // R�cup�ration de l'IP du serveur
    cout << "Quelle est l'IP du serveur?" << endl;
    string IPserv("127.0.0.1");
    cin >> IPserv;

    SOCKET sock;        /* lancement des sockets windows */
    SOCKADDR_IN sin;

    /* Cr�ation de la socket pour le serveur*/
    sock = socket(AF_INET, SOCK_STREAM, 0);
    cout << "Creation socket" << std::endl;

    /* Configuration de la connexion */
    sin.sin_addr.s_addr = inet_addr((const char*)IPserv.data());
    sin.sin_family = AF_INET;
    sin.sin_port = htons(PORT);
    cout << "Connexion sur le port" << endl;

    if(connect(sock, (SOCKADDR*)&sin, sizeof(sin)) != SOCKET_ERROR)
    cout << "Connexion sur le port" << endl;
    else
    cout << "Impossible de se connecter" << endl;

    int flag = 1;
    setsockopt(sock,IPPROTO_TCP,TCP_NODELAY,(char *) &flag,sizeof(int)); //on retire l'algorithme de Nagel pour le transport de donn�es en TCP

    for(int i=0; i<NbArbre; i++) /* recoit les coordonnees des NbArbres (ici 15) arbres*/
    {
        RecevoirEntier(sock, &evthg->arbre[i].posx);
        RecevoirEntier(sock, &evthg->arbre[i].posy);
    }
    RecevoirEntier(sock, &evthg->j1.posx);/* reception des coordonnees en x des joueurs 1 et 2 */
    RecevoirEntier(sock, &evthg->j2.posx);

    while (&evthg->j1.etat || &evthg->j2.etat)
    {

        EnvoyerEntier(sock, evthg->j1.posx); /* le client envoit les coordonnees en x de son personnage */
        RecevoirEntier(sock, &evthg->j2.posx); /* position de joueur 2 */
        for(int i(0); i<NbArbre; i++) /* reception des coordonn�es des arbres*/
        {
            RecevoirEntier(sock, &evthg->arbre[i].posx);
            RecevoirEntier(sock, &evthg->arbre[i].posy);
        }
        if(evthg->arbre[1].posy == evthg->arbre[2].posy){evthg->j2.etat = false;}
        Sleep(16);
    }
    /* On ferme la socket */
    shutdown(sock, 2);
    closesocket(sock);
    return 0;
}
