#ifndef JEU1_H_INCLUDED
#define JEU1_H_INCLUDED


// Ici les prototypes de toutes les fonctions

int EnvoyerEntier(SOCKET sock, int16_t value);
int RecevoirEntier(SOCKET sock, int16_t * pValue);
void* reseau(void* data);


char selection_perso(ALLEGRO_DISPLAY* display);
void jeu (int perso, ALLEGRO_DISPLAY* display);
bool deplacer(ALLEGRO_DISPLAY* display, ALLEGRO_EVENT_QUEUE *event_queue, ALLEGRO_EVENT *event, struct persos *j1, struct persos *j2, bool continuer, int *score, int *score_j1);
int collision(struct general *evthg, int score, int score_j1);
void afficher(ALLEGRO_DISPLAY* display, struct general *evthg, ALLEGRO_BITMAP* img_arbre, ALLEGRO_BITMAP* sprite_perso1, ALLEGRO_BITMAP* sprite_perso2, ALLEGRO_BITMAP* sol, ALLEGRO_BITMAP* img_mouton);
int check_timer(ALLEGRO_TIMER* timer, int score_j1);
void game_is_over(ALLEGRO_DISPLAY* display, int score, int score_j1, int score_j2);

    struct arbres                   // Structure des arbres et du mouton : seulement la position � l'�cran
    {
        int16_t posx, posy;
    };

    struct persos                   // Structure de j1 et j2 : la position et leur �tat (vivant ou mort)
    {
        int16_t posx, posy;
        bool etat;
    };

    struct general                  // Une structure qui englobe les personnages et 20 arbres.
    {                               // Permet d'�tre envoy�e facilement � un thread et par le r�seau.
        struct persos j1, j2;
        struct arbres arbre[20];
    };

#endif // JEU1_H_INCLUDED
