#include <iostream>
#include <stdio.h>
#include <string>
#include "allegro5/allegro.h"
#include "allegro5/allegro_image.h"
#include "allegro5/allegro_primitives.h"
#include <allegro5/allegro_font.h>
#include <allegro5/allegro_ttf.h>
#include <allegro5/allegro_audio.h>
#include <allegro5/allegro_acodec.h>
#include "jeu.h"

#define YOUSHI 0        // Remplace "YOUSHI" par la valeur 0 dans le code.
#define LEIRA 1         // Plus pratique pour se r�p�rer :
#define QUITTER 2       // C'est un rep�re plus concret que des 0, 1 ou 2

using namespace std;



int main(/*int argc, char **argv*/)
{
   srand(time(NULL));           // Permet de g�n�rer un nombre al�atoire parmi un tableau de nombres [rand]

   cout << "Welcome" << endl;

   ALLEGRO_DISPLAY *display = NULL;             // Initialisation de la fen�tre
   ALLEGRO_BITMAP *main_menu = NULL;            // Initialisation de l'image du menu principal
   ALLEGRO_EVENT_QUEUE *event_queue = NULL;     // Initialisation de la queue d'�v�nements

   char perso;                   // Perso peut prendre 3 valeurs, d'o� le char

   if(!al_init())                // On initialise Allegro
   {
       cout << "Failed to initialize Allegro" << endl;
       return -1;
   }

    if(!al_install_keyboard()) {                        // On initialise le clavier
    cout << "Failed to initialize keyboard" << endl;
      return -1;
   }

   al_install_audio();

    if(!al_init_acodec_addon()){
      fprintf(stderr, "failed to initialize audio codecs!\n");
      return -1;
   }

   if (!al_reserve_samples(1)){
      fprintf(stderr, "failed to reserve samples!\n");
      return -1;
   }

    if(!al_install_mouse()) {                               // On Initialisatise la souris
      fprintf(stderr, "failed to initialize the mouse!\n");
      return -1;
   }


   if(!al_init_image_addon()) {                             // On Initialisatise l'addon pour g�rer les images
        cout << "Failed to initialize Image addon" << endl;
      return -1;
   }

    al_init_font_addon();                       // Initialisation de l'addon font
    al_init_ttf_addon();                      // Initialisation de l'addon ttf (True Type Font)

    display = al_create_display(400, 600);   // On cr�e une fen�tre de 400*600 pixels

   if(!display)                                         // On v�rifie que display est bien cr��
   {
       cout << "Failed to initialize Display" << endl;
       return -1;
   }

   main_menu = al_load_bitmap("menu.png");      // On charge l'image de menu principal

    if(!main_menu) {                                    // On v�rifie que l'image a bien �t� charg�e
        cout << "Failed to display main_menu" << endl;
        al_destroy_display(display);                    // On d�truit la fen�tre le cas �ch�ant
        return -1;
   }

      event_queue = al_create_event_queue();            // On cr�e la queue d'�v�nements
   if(!event_queue) {                                   // On v�rifie qu'elle a bien �t� cr��e
      cout << "Failed to create event_queue" << endl;
      al_destroy_bitmap(main_menu);                     // On d�truit image et fen�tre le cas �ch�ant
      al_destroy_display(display);
      return -1;
   }

    al_draw_bitmap(main_menu,0,0,0);                // On buffer (= charge) l'image du menu aux coordonn�es (0;0)

    al_register_event_source(event_queue, al_get_keyboard_event_source());          // On indique que l'�v�nement peut �tre un �v�nement clavier
    al_register_event_source(event_queue, al_get_display_event_source(display));    // On indique que l'�v�nement peut �tre un �v�nement de la fen�tre
    ALLEGRO_EVENT event;            // On cr�e l'�v�nement

    al_flip_display();              // On  rafraichit l'�cran avec les images pr�c�dement charg�es.


    bool done = false;
    while(!done)            // Tant que qu'on a appuy� ni sur echap ni sur enter et qu'on n'a pas ferm� la fen�tre...
    {
        al_wait_for_event(event_queue, &event);     // On attend un �v�nement
		if(event.type == ALLEGRO_EVENT_KEY_DOWN)    // Si on appuie sur une touche clavier...
        {
            if(event.keyboard.keycode == ALLEGRO_KEY_ENTER || event.keyboard.keycode == ALLEGRO_KEY_ESCAPE) // Et qu'il s'agit d'enter ou d'echap
            {
                done = true;            // On quitte la boucle
            }
        }
        else if(event.type == ALLEGRO_EVENT_DISPLAY_CLOSE)  // Idem, mais si on clique sur la croix rouge de la fen�tre
        {
            done = true;
        }
    }
    if(event.keyboard.keycode == ALLEGRO_KEY_ENTER)     // Si la touche appuy�e est "enter"
    {
        perso = selection_perso(display);               // On appelle la fonction de s�lection du personnage
        if(perso == YOUSHI || perso == LEIRA){
            jeu(perso, display);                        // On appelle la fonction de jeu si le joueur a s�lectionn� Youshi ou Leira
        }
    }

   al_destroy_display(display);             // Sinon, on quitte le jeu en d�truisant ce qui a �t� cr��
   al_destroy_bitmap(main_menu);
   al_destroy_event_queue(event_queue);

   return 0;
}
